# **CASE JavaScript Prototyping Boilerplate**
-----
## **Dependencies**
* VirtualBox - https://www.virtualbox.org/wiki/Downloads
* Vagrant - https://www.vagrantup.com/downloads.html
* Node + NPM - http://nodejs.org/download/
* Git - http://git-scm.com/downloads

-----

## **Instructions (first time only)**

In terminal run the following:	

    git clone https://acdtprn@bitbucket.org/acdtprn/proj-case-prototyping-boilerplate.git case-boilerplate
    cd case-boilerplate
    vagrant up

Go and grab a coffee.  When it's finished:

    sudo npm install
    sudo npm install -g nodemon
    sudo nodemon app.js


## **Instructions (all other times)**

In terminal run the following:	

    cd case-boilerplate
    sudo nodemon app.js
_____

## **What you get**

* MySQL (port 3306)
* PostgreSQL (port 5432)
* MongoDB (port 27017)
* Neo4j (port 7474)
* Node (port 3000)
* Frontend Authentication using a Google account

_____

Node doesn't play nicely with Vagrant shared folders, so this is a hybrid solution.  House your project and run node on your local machine, run all other services in a virtual machine.